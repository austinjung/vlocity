var myApp = angular.module('myApp',[]);

myApp.controller("myController", function ($scope, $http) {

    $scope.loadPeople = function() {
        $scope.isLoading = true;
        $http.get("people.json")
        .success(function(data) {
            $scope.people = data.People;
            $scope.personSelected($scope.people[0]);
            $scope.isLoading = false;
        })
        .error(function(data) {
            console.log("No people json");
            $scope.isLoading = false;
        });
    };

    $scope.zipLikeAndDislike = function() {
        var preferences = [];
        var index = 0;
        var likesLength = $scope.selectedPerson.Likes.length;
        var disLikesLength = $scope.selectedPerson.Dislikes.length;
        while (index < likesLength || index < disLikesLength) {
            var preference = {};
            if (index < likesLength) {
                preference.Like = $scope.selectedPerson.Likes[index];
            }
            if (index < disLikesLength) {
                preference.Dislike = $scope.selectedPerson.Dislikes[index];
            }
            preferences[index] = preference;
            index++;
        }
        $scope.selectedPerson.preferences = preferences;
    };

    $scope.personSelected = function(selectedPerson) {
        $scope.selectedPerson = selectedPerson;
        $scope.zipLikeAndDislike();
    };

    $scope.sendMessage = function() {

    };

    $scope.init = function() {
        $scope.loadPeople();
        $scope.search = "";
    };
});
